<HTML>
Hypertext Markup Languange

Pencipta : Tim Berners-Lee
Perusahaan : W3C

http://www.evolutionoftheweb.com/?hl=id
https://www.w3schools.com/

Code Editor 
https://code.visualstudio.com/

<meta charset="UTF-8">
Universal Character Set, atau yang umum disebut sebagai Charset adalah kumpulan dari beberapa jenis pengkodean karakter baik huruf, angka, symbol, dll. Untuk untuk saat ini pengkodean UTF-8 telah menjadi standarisasi untuk pengkodean dalam system operasi, bahasa pemrograman, API, dan software.

KESIMPULAN :
Pada saat membuat satu halaman website dengan memberikan tag <meta charset=”UTF-8”> berarti halaman tersebut telah memberi informasi terhadap browser dan search engine untukmelakukan pengkodean karakter sesuai ketentuan UTF-8.

<meta name="viewport" content="width=device-width, initial-scale=1.0">
Pasangan nilai-kunci width=device-width menyetel lebar tampilan yang terlihat ke lebar perangkat. Pasangan nilai-kunci initial-scale=1 menyetel level zoom awal saat mengunjungi laman.

Struktur HTML
<> = di nama kan Tag
struktur tag 
<namatag atribut="nilai">

- Di dalam <head>
<title></title> = Judul Halaman
<style></style> = CSS
<meta></meta> = metadata

- Di dalam <body>
<h2>, <h2>, <h3>, <h4>, <h5>, <h6>, <p> .... = Teks
<br>, <hr>, <em>, <strong>, ..... = Pendukung teks
<img> = gambar
<a> = hyperlink
<ul>, <ol>, <li>, <dl>, <dt>, <dd> = list (bullets & numbering)
<table>, <thead>, <tbody>, .... = tabel
<form>, <input>, <select>, <button> ... = form
<script> = Javascript
<object> = object
<div>, <span> = grouping
<!-- Ini adalah Komentar. -->

attribut global
- accesskey
- class
- id
- dir
- lang
- style
- tabindex
- title

Judul
Heading = <h1> s/d <h6>
